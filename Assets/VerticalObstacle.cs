﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalObstacle : MonoBehaviour
{
    public float SpeedY = 10;
    float xMov = 0;

    public float upperLimit = 13;
    public float lowerLimit = 3;
    float sign = 1;

    // Update is called once per frame
    void Update()
    {
        float yMov = SpeedY;

        if (transform.position.y >= upperLimit)
            sign = -1;
        if (transform.position.y <= lowerLimit)
            sign = 1;
        yMov = SpeedY * sign;
        Vector3 velocity = new Vector3(xMov, yMov);
        transform.position += velocity * Time.deltaTime;
    }
}

