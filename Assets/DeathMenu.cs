﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DeathMenu : MonoBehaviour
{


    public Image backgroundIMG;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI cointext;
    public TextMeshProUGUI meters;

    private float transition = -0.5f;
    private bool isShown = false;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
     if(!isShown)
        {
            return;
        }


        transition += Time.deltaTime;
        backgroundIMG.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, transition);

    }

    public void ToggleEndMenu(float finalpunt, float CoinScore, float scorepoints)
    {

        gameObject.SetActive(true);
        scoreText.text = ((int)finalpunt).ToString();
        cointext.text = ((int)CoinScore).ToString();
        meters.text = ((int)scorepoints).ToString() + " m";

        isShown = true;
    }
}
