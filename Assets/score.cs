﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class score : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI FinalScore;
    public TextMeshProUGUI CoinText;
    public float finalpunt;
    private float scorepoints = 0.0f;
    private int difficultyLevel = 1;
    private int maxDifficultyLevel = 10;
    private int ScoreToNextLevel = 10;
    private bool isDead = false;
    public DeathMenu deathMenu;
    public float CoinScore;
    // Start is called before the first frame update
    void Start()
    {
        ScoreText.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
            return;

        if (scorepoints >= ScoreToNextLevel)
        {
            LevelUp();
        }
        scorepoints += Time.deltaTime * difficultyLevel ;
        ScoreText.text =((int) scorepoints).ToString() + " m";
        CoinText.text = ((int)CoinScore).ToString();
        FinalScore.text = ((int)finalpunt).ToString();



        finalpunt = scorepoints + (CoinScore * 5);
    }


    void LevelUp()
    {

        if (difficultyLevel == maxDifficultyLevel)
            return;

        ScoreToNextLevel *= 2;
        difficultyLevel++;
      GetComponent<PlayerMotor>().SetSpeed(difficultyLevel);
        Debug.Log(difficultyLevel);
    }


    public void OnDeath()
    {

        isDead = true;
        PlayerPrefs.SetFloat("Highscore", scorepoints);
        deathMenu.ToggleEndMenu(finalpunt, CoinScore, scorepoints);
    }
}
