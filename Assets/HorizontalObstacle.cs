﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalObstacle : MonoBehaviour
{
    public float SpeedX = 10.0f;
    float yMov = 0;

    public float upperLimit = 9;
    public float lowerLimit = -6;
    float sign = 1;

    // Update is called once per frame
    void Update()
    {
        float xMov = SpeedX;

        if (transform.position.x >= upperLimit)
            sign = -1;
        if (transform.position.x <= lowerLimit)
            sign = 1;
        xMov = SpeedX * sign;
        Vector3 velocity = new Vector3(xMov, yMov);
        transform.position += velocity * Time.deltaTime;
    }
}
