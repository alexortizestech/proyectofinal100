﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public GameObject akai;
    public Animator anim;
    public bool trip;
    public AudioSource audioSource;
    public AudioSource ASLoop;
    public AudioClip run;
    public AudioClip CoinS;
    public AudioClip HitSound;
    public AudioClip HoleSound;
    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;
    public int MaxHealth;
    public Rigidbody rb;
    private bool isDead = false;
    private CharacterController controller;
    private Vector3 moveVector;
    public float speed = 5.0f;
    private float verticalVelocity = 0.0f;
    private float gravity = 75.0f;
    float animationDuration = 2.0f;
    private float startTime;
    bool isGrounded;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = akai.GetComponent<Animator>();
        trip = false;
        MaxHealth = 3;
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        startTime = Time.time;
        isGrounded = true;
        ASLoop.Play(0);

    }

    // Update is called once per frame
    void Update()
    {

        if (isDead)
            return;
        
        

        if (Time.time-startTime < animationDuration)
        {
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            return;
        }
        moveVector = Vector3.zero;
        if (controller.isGrounded)
        {
            verticalVelocity = -0.5f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        //x - left & right
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
   


       if (Input.GetMouseButton(0))
        {


            if (Input.mousePosition.x > Screen.width / 2)
                moveVector.x = speed;
            else
                moveVector.x = -speed;
        }

        //z - forward & backward
        moveVector.z = speed;

       

     

        controller.Move((moveVector) * Time.deltaTime);

        if (transform.position.y < -10)
        {
            Death();
        }




        if (MaxHealth <= 0)
        {
            Death();
            anim.Play("Tripping_Complete");
        }



        if (MaxHealth == 2)
        {
            Destroy(Heart3);
        }

        if (MaxHealth == 1)
        {
            Destroy(Heart2);
        }
        if (MaxHealth == 0)
        {
            Destroy(Heart1);
        }

    }

    public void SetSpeed(float modifier)
    {
        speed = 7.5f + modifier;
    }


 

    //Being called everytime character collides
   /* private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.point.z<transform.position.z + controller.radius)
        {
            Death();
        }
      
    }*/




        private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacule")
        {
           
            MaxHealth -= 1;
            // Death();
            if(!isDead)
            audioSource.PlayOneShot(HitSound);
            trip = true;
            anim.Play("Tripping");
        }



        if (other.tag == "Hole")
        {

            audioSource.PlayOneShot(HoleSound);
           // Destroy(gameObject,1.5f);
          Death();
            anim.Play("Falling");
            transform.Translate(Vector3.down * Time.deltaTime);
        }

        if (other.tag == "Coin")
        {
            Destroy(other.gameObject,0);
            GetComponent<score>().CoinScore += 1;

            audioSource.PlayOneShot(CoinS);
        }
        trip = false;
    }


 

    private void Death()
    {
       
        isDead = true;
        GetComponent<score>().OnDeath();

        ASLoop.Pause();
        
    }

}
